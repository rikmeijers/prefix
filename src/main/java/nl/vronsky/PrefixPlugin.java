package nl.vronsky;

import nl.vronsky.moduleManagement.ModuleManager;
import nl.vronsky.modules.example.ExampleModule;
import org.bukkit.plugin.java.JavaPlugin;

public class PrefixPlugin extends JavaPlugin {
    private final ModuleManager<PrefixPlugin> moduleManager = new ModuleManager<>();

    @Override
    public void onLoad() {
        moduleManager.prepare(new ExampleModule(this));
        moduleManager.load();
        System.out.println("Plugin loaded");
    }

    @Override
    public void onEnable() {
        moduleManager.enable();
        System.out.println("Plugin enabled");
    }

    @Override
    public void onDisable() {
        moduleManager.disable();
        System.out.println("Plugin disabled");
    }
}