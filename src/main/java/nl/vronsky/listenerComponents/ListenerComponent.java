package nl.vronsky.listenerComponents;

import nl.vronsky.Log;
import org.bukkit.event.Listener;

public abstract class ListenerComponent implements Listener {
    private final Log logger = new Log();

    protected Log getLogger() {
        return logger;
    }
}
