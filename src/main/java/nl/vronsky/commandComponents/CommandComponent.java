package nl.vronsky.commandComponents;

import nl.vronsky.Log;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

public abstract class CommandComponent extends Command {
    private final String name;
    private final String permission;
    private Player player = null;
    private List<String> subCommands;
    private final Log logger;

    protected Log getLogger() {
        return logger;
    }
    protected void setSubCommands(List<String> subCommands) {
        this.subCommands = subCommands;
    }

    public CommandComponent(String name, String permission) {
        super(name);
        setPermission(permission);

        this.name = name;
        this.permission = permission;
        subCommands = new LinkedList<>();
        logger = new Log();
    }

    protected void addSubCommand(String subCommand) {
        subCommands.add(subCommand);
    }

    protected void clearSubCommands() {
        subCommands.clear();
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public boolean execute(CommandSender sender, String commandLabel, String[] args) {
        if (sender instanceof Player) {
            player = (Player) sender;
        }

        if (!subCommands.isEmpty()) {
            String executedSubcommand = getExecutedSubcommand(args);
            if (!executedSubcommand.isEmpty()) {
                if (hasPermission(player, permission + "." + executedSubcommand)) {
                    try {
                        Method method = getClass().getMethod(executedSubcommand, CommandSender.class, String.class, String[].class);
                        method.invoke(this, sender, commandLabel, getSubCommandArgs(args));
                        return true;
                    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                        logger.logWarning(e.getMessage());
                    }
                }
            }
        }

        if (player != null && !hasPermission(player, permission)) {
            return true;
        }

        onCommand(sender, commandLabel, args);

        return true;
    }

    /**
     * This method is called when the command is executed.
     *
     * @param sender the sender of the command
     * @param commandLabel the label of the command
     * @param args the arguments of the command
     */
    protected abstract void onCommand(CommandSender sender, String commandLabel, String[] args);

    /**
     * Check if a player has the given permission.
     *
     * @param player the player to check
     * @param permission the permission to check
     * @return true if the player has the permission, false otherwise
     */
    protected boolean hasPermission(Player player, String permission) {
        if (player == null) {
            logger.logError("Player is null. Please ensure that a player has been initialized before attempting to access it.");
            return false;
        }

        if (permission == null || permission.isEmpty()) {
            logger.logError("Permissions are null. Please ensure that the permissions have been properly initialized and assigned before attempting to access them.");
            return false;
        }

        if (!player.hasPermission(permission)) {
            player.sendMessage(ChatColor.RED + " You do not have the necessary permissions to perform this action. " +
                    "Please contact your system administrator for assistance.");
            return false;
        }

        return true;
    }

    /**
     * Get the executed subcommand from the given arguments.
     *
     * @param args the arguments of the command
     * @return the executed subcommand, or null if no subcommand was executed
     */
    private String getExecutedSubcommand(String[] args) {
        if (args == null || args.length == 0) {
            return null;
        }

        if (subCommands == null || subCommands.isEmpty()) {
            return null;
        }

        for (String subCommand : subCommands) {
            if (subCommand != null && !subCommand.isEmpty() && subCommand.equalsIgnoreCase(args[0])) {
                return subCommand;
            }
        }

        return null;
    }

    /**
     * Get the arguments for the executed subcommand.
     *
     * @param args the arguments of the command
     * @return the arguments for the executed subcommand, or an empty array if no subcommand was executed
     */
    private String[] getSubCommandArgs(String[] args) {
        if (args == null || args.length == 0) {
            return new String[0];
        }

        if (subCommands == null || subCommands.isEmpty()) {
            return new String[0];
        }

        String[] subCommandArgs = new String[args.length - 1];
        System.arraycopy(args, 1, subCommandArgs, 0, subCommandArgs.length);
        return subCommandArgs;
    }
}
