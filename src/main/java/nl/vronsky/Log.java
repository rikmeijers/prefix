package nl.vronsky;

import org.bukkit.Bukkit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {
    private final Logger logger;

    public Log() {
        logger = Bukkit.getServer().getLogger();
    }

    /**
     * Logs a message at the specified level.
     *
     * @param level the log level
     * @param message the message to log
     */
    public void log(Level level, String message) {
        logger.log(level, message);
    }

    /**
     * Logs an informational message.
     *
     * @param message the message to log
     */
    public void logInfo(String message) {
        logger.log(Level.INFO, message);
    }

    /**
     * Logs a warning message.
     *
     * @param message the message to log
     */
    public void logWarning(String message) {
        logger.log(Level.WARNING, message);
    }


    /**
     * Logs an error message.
     *
     * @param message the message to log
     */
    public void logError(String message) {
        logger.log(Level.SEVERE, message);
    }

    /**
     * Logs a message at the specified level for the specified class.
     *
     * @param clazz the class to log the message for
     * @param level the log level
     * @param message the message to log
     */
    public void logClass(Class<?> clazz, Level level, String message) {
        logger.log(level, clazz.getName() + ": " + message);
    }
}