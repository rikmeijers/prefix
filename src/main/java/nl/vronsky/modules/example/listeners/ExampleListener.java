package nl.vronsky.modules.example.listeners;

import nl.vronsky.listenerComponents.ListenerComponent;
import nl.vronsky.modules.example.ExampleModule;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public class ExampleListener extends ListenerComponent {
//    private final ExampleModule module;

//    public ExampleListener(ExampleModule module) {
//        this.module = module;
//    }

    @EventHandler
    public void playerJoinListener(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.sendMessage(ChatColor.GOLD + "You have joined the server! Thank you for helping developing this framework ;)");

        if (ExampleModule.isThisEnabled) {
//            module.getLogger().logInfo("The boolean 'isThisEnabled' is enabled!");
        }
    }
}
