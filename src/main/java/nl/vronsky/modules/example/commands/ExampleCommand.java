package nl.vronsky.modules.example.commands;

import nl.vronsky.commandComponents.CommandComponent;
import nl.vronsky.modules.example.ExampleModule;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class ExampleCommand extends CommandComponent {
//    private final ExampleModule module;

    public ExampleCommand() {
        super("example", "vronskyframework.example.example");
    }

//    public ExampleCommand(ExampleModule module) {
//        super("example", "vronskyframework.example.example");
//        this.module = module;
//    }



    @Override
    protected void onCommand(CommandSender sender, String commandLabel, String[] args) {
        sender.sendMessage(ChatColor.DARK_AQUA + "The example command is working with the permissions vronskyframework.example.example !");

        if (ExampleModule.isThisEnabled) {
//            module.getLogger().logInfo("The boolean 'isThisEnabled' is enabled!");
        }
    }
}
