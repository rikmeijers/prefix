package nl.vronsky.modules.example;

import nl.vronsky.PrefixPlugin;
import nl.vronsky.moduleComponents.Module;
import nl.vronsky.modules.example.commands.ExampleCommand;
import nl.vronsky.modules.example.listeners.ExampleListener;

public class ExampleModule extends Module<PrefixPlugin> {
    public final static boolean isThisEnabled = true;

    public ExampleModule(PrefixPlugin plugin) {
        super(plugin);
    }

    @Override
    public void onLoad() {
        getLogger().logInfo(getClass().getSimpleName() + " has been LOADED!");
    }

    @Override
    public void onEnable() throws Exception {
        getLogger().logInfo(getClass().getSimpleName() + " has been ENABLED!");

//        do this when you have parameters in your constructor for a command command(() -> new ExampleCommand(this));
//        do this when you have parameters in your constructor for a listener listen(() -> new ExampleListener(this));

        command(ExampleCommand::new);
        listen(ExampleListener::new);
    }

    @Override
    public void onDisable() {
        getLogger().logInfo(getClass().getSimpleName() + " has been DISABLED!");
    }
}
