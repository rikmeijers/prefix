package nl.vronsky;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Database {
    private Connection conn;
    private final Log logger = new Log();

    /**
     * Establishes a connection to the database.
     *
     * @return the established connection
     */
    public Connection connect() throws SQLException {
        Properties properties = getProperties();

        String host = properties.getProperty("db.host");
        String username = properties.getProperty("db.username");
        String password = properties.getProperty("db.password");
        String database = properties.getProperty("db.name");

        String connectionUrl = "jdbc:mysql://" + host + "/" + database;
        conn = DriverManager.getConnection(connectionUrl, username, password);
        return conn;
    }

    /**
     * Closes the connection to the database.
     */
    public void disconnect() throws SQLException {
        if (conn != null) {
            conn.close();
        }
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        try {
            Path gradlePropertiesPath = Paths.get("gradle.properties");
            InputStream gradlePropertiesStream = Files.newInputStream(gradlePropertiesPath);
            properties.load(gradlePropertiesStream);
            gradlePropertiesStream.close();
        } catch (IOException e) {
            logger.logError(e.getMessage());
        }
        return properties;
    }
}
