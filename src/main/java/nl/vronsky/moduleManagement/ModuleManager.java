package nl.vronsky.moduleManagement;

import nl.vronsky.moduleComponents.Module;
import nl.vronsky.moduleComponents.ModuleStatus;
import org.bukkit.plugin.Plugin;

public class ModuleManager<P extends Plugin> {
    private final ModuleManagerProcessor<P> processor;

    public ModuleManager() {
        processor = new ModuleManagerProcessor<>();
    }

    /**
     * Prepares the given module for execution.
     *
     * @param module the module to prepare
     */
    public void prepare(Module<P> module) {
        if (module == null) {
            throw new IllegalArgumentException("Module must not be null to prepare it.");
        }
        if (!processor.processPreparingModule(module)) {
            processor.getLogger().logWarning(module.getName() + " is already prepared.");
        }
    }

    /**
     * Loads the prepared module(s) from storage.
     */
    public void load() {
        if (!processor.processModule(ModuleStatus.LOADED)) {
            throw new IllegalStateException("Cannot load modules because the ModuleManager is not in the IDLE state.");
        }
    }

    /**
     * Enables the prepared module(s) from storage.
     */
    public void enable() {
        if (!processor.processModule(ModuleStatus.ENABLED)) {
            throw new IllegalStateException("Cannot enable modules because the ModuleManager has not been loaded.");
        }
    }

    /**
     * Disables the prepared module(s) from storage.
     */
    public void disable() {
        if (!processor.processModule(ModuleStatus.DISABLED)) {
            throw new IllegalStateException("Cannot disable modules because the ModuleManager has not been enabled.");
        }
    }
}
