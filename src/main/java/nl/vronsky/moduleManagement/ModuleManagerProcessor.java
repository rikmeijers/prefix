package nl.vronsky.moduleManagement;

import nl.vronsky.Log;
import nl.vronsky.moduleComponents.Module;
import nl.vronsky.moduleComponents.ModuleStatus;
import org.bukkit.plugin.Plugin;
import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

class ModuleManagerProcessor<P extends Plugin> {
    private final Map<Class<? extends Module>, Module<P>> modules;
    private final AtomicReference<ModuleStatus> status;
    private final Log logger;

    protected Log getLogger() {
        return logger;
    }

    public ModuleManagerProcessor() {
        modules = new LinkedHashMap<>();
        status = new AtomicReference<>(ModuleStatus.IDLE);
        logger = new Log();
    }

    /**
     * Processes the given module to prepare it for execution.
     *
     * @param module the module to prepare
     * @return true if the module was prepared successfully, false otherwise
     */
    protected boolean processPreparingModule(@Nonnull Module<P> module) {
        Class<? extends Module> moduleClass = module.getClass();
        if (!modules.containsKey(moduleClass)) {
            modules.put(moduleClass, module);
            return true;
        }
        return false;
    }

    /**
     * This method processes a module.
     *
     * @param status The status of the module to be processed.
     * @return A boolean value indicating the success or failure of the processing.
     */
    protected boolean processModule(ModuleStatus status) {
        switch (status) {
            case LOADED:
                return processModuleLoading();
            case ENABLED:
                return processModuleEnabling();
            case DISABLED:
                return processModuleDisabling();
            default:
                return false;
        }
    }

    /**
     * This method processes the loading of a module.
     *
     * @return A boolean value indicating the success or failure of the module loading.
     */
    private boolean processModuleLoading() {
        if (!status.compareAndSet(ModuleStatus.IDLE, ModuleStatus.LOADED)) {
            return false;
        }

        for (Module<P> module : modules.values()) {
            try {
                module.load();
            } catch (Exception e) {
                logger.logError("Error loading module: " + module + ", Error:");
                logger.logError(e.getMessage());
            }
        }
        return true;
    }

    /**
     * This method processes the enabling of a module.
     *
     * @return A boolean value indicating the success or failure of the module enabling.
     */
    private boolean processModuleEnabling() {
        if (!status.compareAndSet(ModuleStatus.LOADED, ModuleStatus.ENABLED)) {
            return false;
        }

        for (Module<P> module : modules.values()) {
            try {
                if (!module.enable()) {
                    return false;
                }
            } catch (Exception e) {
                logger.logError("Error enabling module: " + module + ", Error:");
                logger.logError(e.getMessage());
                return false;
            }
        }
        return true;
    }


    /**
     * This method processes the disabling of a module.
     *
     * @return A boolean value indicating the success or failure of the module disabling.
     */
    private boolean processModuleDisabling() {
        if (!status.compareAndSet(ModuleStatus.ENABLED, ModuleStatus.DISABLED)) {
            return false;
        }

        for (Module<P> module : modules.values()) {
            try {
                module.disable();
            } catch (Exception e) {
                logger.logError("Error disabling module: " + module + ", Error:");
                logger.logError(e.getMessage());
            }
        }
        return true;
    }
}
