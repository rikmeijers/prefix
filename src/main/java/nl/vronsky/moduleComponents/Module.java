package nl.vronsky.moduleComponents;

import nl.vronsky.Log;
import nl.vronsky.commandComponents.CommandComponent;
import nl.vronsky.listenerComponents.ListenerComponent;
import org.bukkit.plugin.Plugin;
import java.util.function.Supplier;

public abstract class Module<P extends Plugin> implements StartingMethods {
    private final P plugin;
    private final String name;
    private final ModuleProcessor processor;

    public Plugin getPlugin() {
        return plugin;
    }
    public String getName() {
        return name;
    }
    public Log getLogger() {
        return processor.getLogger();
    }

    public Module(P plugin) {
        this.plugin = plugin;
        name = getClass().getSimpleName();
        processor = new ModuleProcessor(this);
    }

    /**
     * Attempts to load the module. This method will call the `onLoad()`
     * method to initialize the module, and will set the module's status
     * to "loaded" if the initialization is successful.
     */
    public void load() {
        processor.processModule(ModuleStatus.LOADED);
    }

    /**
     * Attempts to enable the module. This method will call the `onEnable()`
     * method to start any background tasks or services that the module
     * needs to function properly, and will set the module's status to
     * "enabled" if the enable operation is successful.
     *
     * @return true if the module was successfully enabled, false otherwise
     */
    public boolean enable() {
        processor.processModule(ModuleStatus.ENABLED);
        return true;
    }

    /**
     * Attempts to disable the module. This method will call the `onDisable()`
     * method to stop any background tasks or services that the module was
     * using, and will set the module's status to "disabled" if the disable
     * operation is successful.
     */
    public void disable() {
        processor.processModule(ModuleStatus.DISABLED);
    }

    /**
     * Tries to instantiate a listener.
     *
     * @param supplier a supplier that provides the listener to instantiate
     * @throws IllegalStateException if the module is not enabled
     */
    protected void listen(Supplier<? extends ListenerComponent> supplier) {
        boolean status = processor.getModuleUtil().checkStatusOfModule(ModuleStatus.ENABLED);
        if (!status) {
            throw new IllegalStateException("Cannot register listener because module " + name + " is not enabled.");
        }

        processor.getModuleUtil().addListener(supplier);
    }

    /**
     * Tries to instantiate a command.
     *
     * @param supplier a supplier that provides the command to instantiate
     * @throws IllegalStateException if the module is not enabled
     */
    protected void command(Supplier<? extends CommandComponent> supplier) {
        boolean status = processor.getModuleUtil().checkStatusOfModule(ModuleStatus.ENABLED);
        if (!status) {
            throw new IllegalStateException("Cannot register command because module " + name + " is not enabled.");
        }

        processor.getModuleUtil().addCommand(supplier);
    }
}
