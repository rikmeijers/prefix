package nl.vronsky.moduleComponents;

import nl.vronsky.Log;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import java.util.LinkedList;
import java.util.List;

class ModuleProcessor {
    private final Module<? extends Plugin> module;
    private final ModuleUtil moduleUtil;
    private ModuleStatus moduleStatus;
    private CommandMap commandMap;
    private final List<Listener> listeners;
    private final List<Command> commands;
    private final Log logger;

    protected Module<? extends Plugin> getModule() {
        return module;
    }
    protected ModuleUtil getModuleUtil() {
        return moduleUtil;
    }
    protected ModuleStatus getModuleStatus() {
        return moduleStatus;
    }
    protected CommandMap getCommandMap() {
        return commandMap;
    }
    protected List<Listener> getListeners() {
        return listeners;
    }
    protected List<Command> getCommands() {
        return commands;
    }
    protected Log getLogger() {
        return logger;
    }
    protected void setModuleStatus(ModuleStatus moduleStatus) {
        this.moduleStatus = moduleStatus;
    }
    protected void setCommandMap(CommandMap commandMap) {
        this.commandMap = commandMap;
    }

    public ModuleProcessor(Module<? extends Plugin> module) {
        this.module = module;
        moduleUtil = new ModuleUtil(this);
        moduleStatus = ModuleStatus.IDLE;
        listeners = new LinkedList<>();
        commands = new LinkedList<>();
        logger = new Log();
    }

    protected void clearListeners() {
        listeners.clear();
    }

    protected void clearCommands() {
        commands.clear();
    }

    /**
     * This method processes a module.
     *
     * @param moduleStatus The status of the module to be processed.
     */
    protected void processModule(ModuleStatus moduleStatus) {
        switch (moduleStatus) {
            case LOADED:
                processLoadingModule();
                break;
            case ENABLED:
                processEnablingModule();
                break;
            case DISABLED:
                processDisablingModule();
                break;
            default:
                throw new IllegalArgumentException("Invalid module status: " + moduleStatus);
        }
    }

    /**
     * This method processes the loading of a module.
     */
    private void processLoadingModule() {
        boolean status = moduleUtil.checkStatusOfModule(ModuleStatus.IDLE);
        if (!status) {
            return;
        }

        module.onLoad();
        setModuleStatus(ModuleStatus.LOADED);
        logger.logInfo("Loading module: " + module.getName());
    }

    /**
     * This method processes the enabling of a module.
     */
    private void processEnablingModule() {
        boolean status = moduleUtil.checkStatusOfModule(ModuleStatus.LOADED);
        if (!status) {
            return;
        }

        setModuleStatus(ModuleStatus.ENABLED);
        moduleUtil.tryEnablingModule();
        logger.logInfo("Enabling module: " + module.getName());
    }


    /**
     * This method processes the disabling of a module.
     */
    private void processDisablingModule() {
        boolean status = moduleUtil.checkStatusOfModule(ModuleStatus.ENABLED);
        if (!status) {
            return;
        }

        int amountOfComponents = getListeners().size() + getCommands().size();
        logger.logInfo("Disabling module: " + module.getName() + ", removing " + amountOfComponents + " components...");

        moduleUtil.disableListeners();
        moduleUtil.disableCommands();
        module.onDisable();
        setModuleStatus(ModuleStatus.DISABLED);
    }
}
