package nl.vronsky.moduleComponents;

interface StartingMethods {
    /**
     * Called when the module is loaded. This method is responsible for
     * initializing the module, such as setting up any data structures or
     * connections to external resources.
     */
    void onLoad();

    /**
     * Called when the module is enabled. This method is responsible for
     * starting any background tasks or services that the module needs to
     * function properly.
     *
     * @throws Exception if there is an error while enabling the module
     */
    void onEnable() throws Exception;

    /**
     * Called when the module is disabled. This method is responsible for
     * stopping any background tasks or services that the module was using,
     * and for cleaning up any resources that the module was using.
     */
    void onDisable();
}
