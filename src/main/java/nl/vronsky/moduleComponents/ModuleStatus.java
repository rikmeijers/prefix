package nl.vronsky.moduleComponents;

public enum ModuleStatus {
    IDLE(0),
    LOADED(1),
    ENABLED(2),
    DISABLED(3),
    UNKNOWN(4);

    private final int value;

    ModuleStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ModuleStatus getModuleStatus(int value) {
        for (ModuleStatus moduleStatus : ModuleStatus.values()) {
            if (moduleStatus.getValue() == value) {
                return moduleStatus;
            }
        }
        return ModuleStatus.UNKNOWN;
    }
}
