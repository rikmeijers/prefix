package nl.vronsky.moduleComponents;

import nl.vronsky.commandComponents.CommandComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

class ModuleUtil {
    private final ModuleProcessor processor;

    public ModuleUtil(ModuleProcessor processor) {
        this.processor = processor;
    }

    /**
     * Checks the status of a module.
     *
     * @param statusToCheckFor the status of the module to check
     * @return true if the module status is correct, false otherwise
     */
    public boolean checkStatusOfModule(ModuleStatus statusToCheckFor) {
        ModuleStatus currentStatus = processor.getModuleStatus();
        if (currentStatus == null) {
            return false;
        }
        return currentStatus.equals(statusToCheckFor);
    }

    /**
     * Tries to enable a module.
     *
     * @return true if the module was successfully enabled, false otherwise
     */
    public boolean tryEnablingModule() {
        try {
            processor.getModule().onEnable();
            processor.setModuleStatus(ModuleStatus.ENABLED);
        } catch (Exception e) {
            processor.getLogger().logError(e.getMessage());
            Bukkit.shutdown();
            return false;
        }
        return true;
    }

    /**
     * Disables all listeners.
     */
    public void disableListeners() {
        List<Listener> listeners = processor.getListeners();
        listeners.forEach(HandlerList::unregisterAll);
        processor.clearListeners();
    }

    /**
     * Disables all commands.
     */
    public void disableCommands() {
        List<Command> commands = processor.getCommands();
        CommandMap commandMap;
        try {
            if (processor.getCommandMap() == null) {
                commandMap = (CommandMap) Bukkit.getServer().getClass().getDeclaredMethod("getCommandMap").invoke(Bukkit.getServer());
                processor.setCommandMap(commandMap);
            } else {
                commandMap = processor.getCommandMap();
            }
        } catch (Exception e) {
            processor.getLogger().logError("Could not load the CommandMap, Error:");
            processor.getLogger().logError(e.getMessage());
            return;
        }

        for (Command command : commands) {
            command.unregister(commandMap);
        }
        commands.clear();
        processor.clearCommands();
    }

    /**
     * Adds a listener to the list of listeners that will be notified when
     * events of the specified type occur.
     *
     * @param supplier a supplier that provides the listener to instantiate
     */
    public void addListener(Supplier<? extends Listener> supplier) {
        Objects.requireNonNull(supplier, "Supplier should not be null.");
        Listener listener = Objects.requireNonNull(supplier.get(), "Supplied listener should not be null.");

        Plugin plugin = processor.getModule().getPlugin();
        Objects.requireNonNull(plugin, "Module plugin should not be null.");

        PluginManager pluginManager = plugin.getServer().getPluginManager();
        Objects.requireNonNull(pluginManager, "Plugin manager should not be null.");

        pluginManager.registerEvents(listener, plugin);
        processor.getListeners().add(listener);
    }


    /**
     * Adds a command to the list of commands that can be executed by the user.
     *
     * @param supplier a supplier that provides the command to instantiate
     */
    public void addCommand(Supplier<? extends CommandComponent> supplier) {
        Objects.requireNonNull(supplier, "Supplier should not be null.");
        Command command = Objects.requireNonNull(supplier.get(), "Supplied command should not be null.");
        String name = supplier.get().getName();

        Plugin plugin = processor.getModule().getPlugin();
        Objects.requireNonNull(plugin, "Module plugin should not be null.");

        CommandMap commandMap;
        try {
            if (processor.getCommandMap() == null) {
                commandMap = (CommandMap) Bukkit.getServer().getClass().getDeclaredMethod("getCommandMap").invoke(Bukkit.getServer());
                processor.setCommandMap(commandMap);
            } else {
                commandMap = processor.getCommandMap();
            }
        } catch (Exception e) {
            processor.getLogger().logError("Could not load the CommandMap, Error:");
            processor.getLogger().logError(e.getMessage());
            return;
        }
        commandMap.register(name, command);
        processor.getCommands().add(command);
    }

}
