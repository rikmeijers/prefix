# Prefix (VronskyFramework)

Looking for a top-notch prefix, chat, and tablist plugin? Look no further! This plugin not only allows you to customize the prefix and tablist, but also includes an automatic update feature for the tablist. You can easily change everything using commands or through the configuration files. In addition, there is a custom event in the chat that makes the chat more visually appealing by displaying your name in bold when it's mentioned by others.

## Installation

1. Make sure you have [Java JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) installed.
2. Download and install [IntelliJ IDEA](https://www.jetbrains.com/idea/download/).
3. Install the [SpotBugs](https://spotbugs.github.io/) plugin for IntelliJ IDEA.

## Building and Testing

To build the project from source:

```shell
gradle clean build
```

To run tests:

```shell
gradle :tests
```

To remove build artifacts:

```shell
gradle clean
```

## License

See [LICENSE](/LICENSE)
